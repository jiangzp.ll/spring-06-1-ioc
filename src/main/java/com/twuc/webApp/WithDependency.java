package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private final Dependent dependent;

    public WithDependency(Dependent dependent) {
        this.dependent = dependent;
    }

    public Dependent getDependent() {
        return dependent;
    }
}

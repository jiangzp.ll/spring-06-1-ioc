### 2.1 `AnnotationConfigApplicationContext` 的使用
1. should create object without dependency.
2. should create object with dependency.
3. should throw exception when the class out of scanning range.
4. should get object by the implemented interface.
5. should get object by the implemented interface using factory.
### 2.2 `AnnotationConfigApplicationContext` 其他用例
1. should create object with the first constructor.
2. should call constructor and autowired.
3. should get multiple instance.

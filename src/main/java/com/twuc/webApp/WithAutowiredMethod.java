package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WithAutowiredMethod {
    private final Dependent dependent;
    private AnotherDependent anotherDependent;
    List<String> logs = new ArrayList<String>();

    public WithAutowiredMethod(Dependent dependent, AnotherDependent anotherDependent) {
        StringBuffer buffer = new StringBuffer();

        this.dependent = dependent;
        this.anotherDependent = anotherDependent;

        buffer.append("WithAutowiredMethod()");
        if (dependent == null) {
            buffer.append(" null ");
        }
        if (anotherDependent == null) {
            buffer.append(" null ");
        }
        logs.add(buffer.toString());
    }

    public Dependent getDependent() {
        return dependent;
    }

    @Autowired
    public void initialize(AnotherDependent another) {
        StringBuffer buffer = new StringBuffer();

        buffer.append("initialize()");
        if (another == null) {
            buffer.append(" null ");
        }
        logs.add(buffer.toString());
    }

    public List<String> getLogs() {
        return logs;
    }
}

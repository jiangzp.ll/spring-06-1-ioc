package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependent dependent;
    private String name;

    @Autowired
    public MultipleConstructor(Dependent dependent) {
        this.dependent = dependent;
    }

    public MultipleConstructor(Dependent dependent, String name) {
        this.dependent = dependent;
        this.name = name;
    }

    public Dependent getDependent() {
        return dependent;
    }

    public String getName() {
        return name;
    }
}
